<?php
/**
 * @file
 * ds_features_views_list_of_nodes2.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ds_features_views_list_of_nodes2_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
