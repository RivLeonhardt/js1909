<?php
/**
 * @file
 * ds_features_views_entity.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function ds_features_views_entity_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'entitystorage';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'storage';
  $view->human_name = 'entityStorage';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Entity Storage';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: My entity view: SID */
  $handler->display->display_options['fields']['sid']['id'] = 'sid';
  $handler->display->display_options['fields']['sid']['table'] = 'storage';
  $handler->display->display_options['fields']['sid']['field'] = 'sid';
  $handler->display->display_options['fields']['sid']['label'] = '';
  $handler->display->display_options['fields']['sid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['sid']['element_default_classes'] = FALSE;
  /* Field: My entity view: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'storage';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  /* Field: My entity view: gender */
  $handler->display->display_options['fields']['gender']['id'] = 'gender';
  $handler->display->display_options['fields']['gender']['table'] = 'storage';
  $handler->display->display_options['fields']['gender']['field'] = 'gender';
  $handler->display->display_options['fields']['gender']['label'] = 'Gender';
  /* Field: My entity view: prefix */
  $handler->display->display_options['fields']['prefix']['id'] = 'prefix';
  $handler->display->display_options['fields']['prefix']['table'] = 'storage';
  $handler->display->display_options['fields']['prefix']['field'] = 'prefix';
  $handler->display->display_options['fields']['prefix']['label'] = 'Prefix';
  /* Field: My entity view: Age */
  $handler->display->display_options['fields']['age']['id'] = 'age';
  $handler->display->display_options['fields']['age']['table'] = 'storage';
  $handler->display->display_options['fields']['age']['field'] = 'age';
  /* Field: My entity view: Father */
  $handler->display->display_options['fields']['father']['id'] = 'father';
  $handler->display->display_options['fields']['father']['table'] = 'storage';
  $handler->display->display_options['fields']['father']['field'] = 'father';
  /* Field: My entity view: Mother */
  $handler->display->display_options['fields']['mother']['id'] = 'mother';
  $handler->display->display_options['fields']['mother']['table'] = 'storage';
  $handler->display->display_options['fields']['mother']['field'] = 'mother';
  /* Field: My entity view: Mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'storage';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  /* Field: My entity view: Pets */
  $handler->display->display_options['fields']['pets']['id'] = 'pets';
  $handler->display->display_options['fields']['pets']['table'] = 'storage';
  $handler->display->display_options['fields']['pets']['field'] = 'pets';
  /* Field: Bulk operations: My entity view */
  $handler->display->display_options['fields']['views_bulk_operations']['id'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['table'] = 'storage';
  $handler->display->display_options['fields']['views_bulk_operations']['field'] = 'views_bulk_operations';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['display_type'] = '0';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['enable_select_all_pages'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['row_clickable'] = 1;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['force_single'] = 0;
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_settings']['entity_load_capacity'] = '10';
  $handler->display->display_options['fields']['views_bulk_operations']['vbo_operations'] = array(
    'action::entity_pos_change_gender_to_female_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
    'action::entity_pos_change_gender_to_male_action' => array(
      'selected' => 1,
      'postpone_processing' => 0,
      'skip_confirmation' => 1,
      'override_label' => 0,
      'label' => '',
    ),
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'entity_storage';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'EntityView';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $handler->display->display_options['menu']['context_only_inline'] = 0;
  $translatables['entitystorage'] = array(
    t('Master'),
    t('Entity Storage'),
    t('more'),
    t('Apply'),
    t('Reset'),
    t('Sort by'),
    t('Asc'),
    t('Desc'),
    t('Items per page'),
    t('- All -'),
    t('Offset'),
    t('« first'),
    t('‹ previous'),
    t('next ›'),
    t('last »'),
    t('.'),
    t(','),
    t('Name'),
    t('Gender'),
    t('Prefix'),
    t('Age'),
    t('Father'),
    t('Mother'),
    t('Mail'),
    t('Pets'),
    t('My entity view'),
    t('- Choose an operation -'),
    t('Page'),
  );
  $export['entitystorage'] = $view;

  return $export;
}
