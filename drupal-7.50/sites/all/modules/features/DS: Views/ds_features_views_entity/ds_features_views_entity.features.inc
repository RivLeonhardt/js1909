<?php
/**
 * @file
 * ds_features_views_entity.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ds_features_views_entity_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
