<?php
/**
 * @file
 * ds_features_voc_weapon.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ds_features_voc_weapon_taxonomy_default_vocabularies() {
  return array(
    'weapon' => array(
      'name' => 'Weapon',
      'machine_name' => 'weapon',
      'description' => 'different types of weapon',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
