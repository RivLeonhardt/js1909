<?php
/**
 * @file
 * ds_features_ct_basic_page.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ds_features_ct_basic_page_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_template'.
  $field_bases['field_template'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_template',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'list',
    'settings' => array(
      'allowed_values' => array(
        'faq' => 'FAQ',
        'sitemap' => 'SiteMap',
      ),
      'allowed_values_function' => '',
    ),
    'translatable' => 0,
    'type' => 'list_text',
  );

  return $field_bases;
}
