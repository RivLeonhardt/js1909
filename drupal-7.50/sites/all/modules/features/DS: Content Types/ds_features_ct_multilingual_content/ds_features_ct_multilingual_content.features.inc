<?php
/**
 * @file
 * ds_features_ct_multilingual_content.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ds_features_ct_multilingual_content_node_info() {
  $items = array(
    'multilingualcontent' => array(
      'name' => t('MultiLingualContent'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
