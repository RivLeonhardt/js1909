<?php
/**
 * @file
 * ds_features_ct_myct.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ds_features_ct_myct_node_info() {
  $items = array(
    'myct' => array(
      'name' => t('MyCT'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
