<?php
/**
 * @file
 * ds_features_ct_youtube.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ds_features_ct_youtube_node_info() {
  $items = array(
    'linktube' => array(
      'name' => t('LinkTube'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
