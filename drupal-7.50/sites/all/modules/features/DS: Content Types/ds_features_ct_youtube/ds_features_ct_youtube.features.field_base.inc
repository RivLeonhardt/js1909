<?php
/**
 * @file
 * ds_features_ct_youtube.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function ds_features_ct_youtube_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_link'.
  $field_bases['field_link'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_link',
    'indexes' => array(),
    'locked' => 0,
    'module' => 'link_tube',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'link_tube_field',
  );

  return $field_bases;
}
