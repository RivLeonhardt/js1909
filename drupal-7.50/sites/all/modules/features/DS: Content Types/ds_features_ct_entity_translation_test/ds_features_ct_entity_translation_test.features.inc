<?php
/**
 * @file
 * ds_features_ct_entity_translation_test.features.inc
 */

/**
 * Implements hook_node_info().
 */
function ds_features_ct_entity_translation_test_node_info() {
  $items = array(
    'entitytranslationtest' => array(
      'name' => t('EntityTranslationTest'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
