<?php
/**
 * @file
 * ds_features_lang_french.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function ds_features_lang_french_locale_default_languages() {
  $languages = array();

  // Exported language: fr.
  $languages['fr'] = array(
    'language' => 'fr',
    'name' => 'French',
    'native' => 'Français',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => 'http://fr.drupal7.local/node',
    'prefix' => '',
    'weight' => 0,
  );
  return $languages;
}
