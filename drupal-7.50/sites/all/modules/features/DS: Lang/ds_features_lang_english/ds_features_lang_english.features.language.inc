<?php
/**
 * @file
 * ds_features_lang_english.features.language.inc
 */

/**
 * Implements hook_locale_default_languages().
 */
function ds_features_lang_english_locale_default_languages() {
  $languages = array();

  // Exported language: en.
  $languages['en'] = array(
    'language' => 'en',
    'name' => 'English',
    'native' => 'English',
    'direction' => 0,
    'enabled' => 1,
    'plurals' => 0,
    'formula' => '',
    'domain' => 'http://drupal7.local/node',
    'prefix' => '',
    'weight' => 0,
  );
  return $languages;
}
