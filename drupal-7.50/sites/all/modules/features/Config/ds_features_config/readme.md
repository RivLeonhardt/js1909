make pets owners form on 2 steps. set firstname and lastname on first step, other items on the second step

25.10
1. Ctools 
    a) create custom content plugin for panels that will display dummy text
(filled in configuration form of this plugin);
    B) create access plugin that will hide your content plugin for authenticated users.
    C) Создать custom content type pane с optional context и в render callback получить данные из ноды(контекста) и вывести

Module: myplugin

2. Views customization
    a) integrate “pets_owners_storage” to views;
    b) create custom views handler that will provide filter by prefix field. It should be dropdown with 2 values: 
    female (for mrs and ms values), male (mr value);
    c) create the view that uses your custom filter and display data from the “pets_owners_storage” table.
    Use table display;
    d) create  custom VBO actions that will change the gender for selected rows to male and female (two actions needed);
    e) theme your view, remove all drupal wrappers that are generated for the field. <table>,
    <p> and <span> should be used only.

Module: entity_pos (actions, views)

3. Web Services
Create simple API for “pets_owners_storage” using Services module
(we need to have possibility to load, edit, delete every record and get the list).

Module: entity_pos (resources)

4. Webform, EntityForm
a) create custom component for webform that will show two selectboxes: color and size (the lists are predefined);
b) create webform that will use your component;
c) create the same using entityForm.

Modules: webform_select (custom webform component), ef_select (field for entity form).

