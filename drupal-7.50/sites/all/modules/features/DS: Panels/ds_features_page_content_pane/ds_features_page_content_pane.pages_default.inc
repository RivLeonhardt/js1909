<?php
/**
 * @file
 * ds_features_page_content_pane.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ds_features_page_content_pane_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'content';
  $page->task = 'page';
  $page->admin_title = 'Content';
  $page->admin_description = '';
  $page->path = 'contentpanel';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'content_pane',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_content__panel';
  $handler->task = 'page';
  $handler->subtask = 'content';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c673f3aa-9d0f-47b3-b6a3-01f0ab6212cf';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_content__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-1dafb75e-17c5-4afe-99a3-d74398a34c97';
  $pane->panel = 'center';
  $pane->type = 'myplugin';
  $pane->subtype = 'myplugin';
  $pane->shown = TRUE;
  $pane->access = array(
    'plugins' => array(
      0 => array(
        'name' => 'myaccess',
        'settings' => array(
          'anon' => 1,
        ),
        'not' => FALSE,
      ),
    ),
  );
  $pane->configuration = array(
    'name' => 'test name',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '1dafb75e-17c5-4afe-99a3-d74398a34c97';
  $display->content['new-1dafb75e-17c5-4afe-99a3-d74398a34c97'] = $pane;
  $display->panels['center'][0] = 'new-1dafb75e-17c5-4afe-99a3-d74398a34c97';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['content'] = $page;

  return $pages;

}
