<?php
/**
 * @file
 * ds_features_page_panel2.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ds_features_page_panel2_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
