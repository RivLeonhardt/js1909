<?php
/**
 * @file
 * ds_features_page_panel1.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ds_features_page_panel1_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'nodes';
  $page->task = 'page';
  $page->admin_title = 'panel1';
  $page->admin_description = '';
  $page->path = 'panel1';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Panel1',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_nodes__panel';
  $handler->task = 'page';
  $handler->subtask = 'nodes';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'onecol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'middle' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '93ff335f-4ef4-4429-a720-c471ca5dd23b';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_nodes__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-04e0453d-3a6b-45c5-b5ae-255197014658';
  $pane->panel = 'middle';
  $pane->type = 'block';
  $pane->subtype = 'views--exp-show_nodes-page';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'inherit_path' => 0,
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
    'style' => 'default',
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '04e0453d-3a6b-45c5-b5ae-255197014658';
  $display->content['new-04e0453d-3a6b-45c5-b5ae-255197014658'] = $pane;
  $display->panels['middle'][0] = 'new-04e0453d-3a6b-45c5-b5ae-255197014658';
  $pane = new stdClass();
  $pane->pid = 'new-0f7bdd0e-92b2-4c0d-9316-52dfff02d3ff';
  $pane->panel = 'middle';
  $pane->type = 'views';
  $pane->subtype = 'show_nodes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'page',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 1;
  $pane->locks = array();
  $pane->uuid = '0f7bdd0e-92b2-4c0d-9316-52dfff02d3ff';
  $display->content['new-0f7bdd0e-92b2-4c0d-9316-52dfff02d3ff'] = $pane;
  $display->panels['middle'][1] = 'new-0f7bdd0e-92b2-4c0d-9316-52dfff02d3ff';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = 'new-0f7bdd0e-92b2-4c0d-9316-52dfff02d3ff';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['nodes'] = $page;

  return $pages;

}
