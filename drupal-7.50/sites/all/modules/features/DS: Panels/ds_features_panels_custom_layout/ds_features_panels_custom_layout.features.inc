<?php
/**
 * @file
 * ds_features_panels_custom_layout.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ds_features_panels_custom_layout_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}
