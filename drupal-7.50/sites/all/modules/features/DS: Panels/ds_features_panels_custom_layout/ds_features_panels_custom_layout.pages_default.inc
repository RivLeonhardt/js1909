<?php
/**
 * @file
 * ds_features_panels_custom_layout.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function ds_features_panels_custom_layout_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'custom_layout';
  $page->task = 'page';
  $page->admin_title = 'Custom layout';
  $page->admin_description = '';
  $page->path = 'custom_layout';
  $page->access = array();
  $page->menu = array(
    'type' => 'normal',
    'title' => 'Custom layout',
    'name' => 'navigation',
    'weight' => '0',
    'parent' => array(
      'type' => 'none',
      'title' => '',
      'name' => 'navigation',
      'weight' => '0',
    ),
  );
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_custom_layout__panel';
  $handler->task = 'page';
  $handler->subtask = 'custom_layout';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'clayout';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'c1ee6907-9e9c-451a-95d7-e897c9bb3efd';
  $display->storage_type = 'page_manager';
  $display->storage_id = 'page_custom_layout__panel';
  $display->content = array();
  $display->panels = array();
  $pane = new stdClass();
  $pane->pid = 'new-8b6667b8-7054-42fc-b27b-0b84d092be25';
  $pane->panel = 'left';
  $pane->type = 'views';
  $pane->subtype = 'show_nodes';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'override_pager_settings' => 0,
    'use_pager' => 1,
    'nodes_per_page' => '10',
    'pager_id' => '0',
    'offset' => '0',
    'more_link' => 0,
    'feed_icons' => 0,
    'panel_args' => 0,
    'link_to_view' => 0,
    'args' => '',
    'url' => '',
    'display' => 'page',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = '8b6667b8-7054-42fc-b27b-0b84d092be25';
  $display->content['new-8b6667b8-7054-42fc-b27b-0b84d092be25'] = $pane;
  $display->panels['left'][0] = 'new-8b6667b8-7054-42fc-b27b-0b84d092be25';
  $pane = new stdClass();
  $pane->pid = 'new-d21a7349-f599-48ee-842c-b00647dea01e';
  $pane->panel = 'right';
  $pane->type = 'search_form';
  $pane->subtype = 'search_form';
  $pane->shown = TRUE;
  $pane->access = array();
  $pane->configuration = array(
    'type' => 'node',
    'form' => 'advanced',
    'path_type' => 'default',
    'path' => '',
    'override_prompt' => 0,
    'prompt' => '',
    'context' => 'empty',
    'override_title' => 0,
    'override_title_text' => '',
    'override_title_heading' => 'h2',
  );
  $pane->cache = array();
  $pane->style = array(
    'settings' => NULL,
  );
  $pane->css = array();
  $pane->extras = array();
  $pane->position = 0;
  $pane->locks = array();
  $pane->uuid = 'd21a7349-f599-48ee-842c-b00647dea01e';
  $display->content['new-d21a7349-f599-48ee-842c-b00647dea01e'] = $pane;
  $display->panels['right'][0] = 'new-d21a7349-f599-48ee-842c-b00647dea01e';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-8b6667b8-7054-42fc-b27b-0b84d092be25';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['custom_layout'] = $page;

  return $pages;

}
