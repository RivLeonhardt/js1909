<?php
/**
 * @file
 * Select component definition.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_double_select() {
  return array(
    'name'     => '',
    'form_key' => NULL,
    'pid'      => 0,
    'weight'   => 0,
    'value'    => '0',
    'extra'    => array(
      'items'        => '',
      'field_prefix' => '',
      'field_suffix' => '',
      'colors' => [
        'white' => 'white',
        'green' => 'green',
        'blue'  => 'blue',
        'red'   => 'red',
      ],
      'sizes'  => [
        'small'  => '2.5',
        'medium' => '3.5',
        'high'   => '4.7',
        'high+'  => '5',
      ],
    ),
  );
}

/**
 * Implements _webform_theme_component().
 */
function _webform_theme_double_select() {
  return array(
    'webform_display_double_select' => array(
      'render element' => 'element',
    ),
    'double_select' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_double_select($component) {
  $form = [];
  $form['items'] = array(
    '#type' => 'textfield',
    '#title' => t('test'),
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_double_select($component, $value = NULL, $filter = TRUE, $submission = NULL) {
  $element = array();
  $component['value'] = $value;
  $element['double_select'] = array(
    '#type'          => 'double_select',
    '#title'         => t('double select'),
    '#title_display' => 'before',
    '#default_value' => $component,
    '#weight'        => 0,
    '#translatable'  => array('title'),
  );
  return $element;
}

/**
 * Webform_select_element process callback().
 */
function double_select($element, $value = NULL, $filter = TRUE, $submission = NULL) {
  $data = $element['#default_value'];
  $node = isset($data['nid']) ? node_load($data['nid']) : NULL;
  $element['color'] = array(
    '#type'           => 'select',
    '#title'          => t('Color'),
    '#title_display'  => 'before',
    '#options'        => $data['extra']['colors'],
    '#default_value'  => $data['value'] ? $data['value'] : NULL,
    '#field_prefix'   => empty($data['extra']['field_prefix']) ? NULL : ($filter ? webform_filter_xss($data['extra']['field_prefix']) : $data['extra']['field_prefix']),
    '#field_suffix'   => empty($data['extra']['field_suffix']) ? NULL : ($filter ? webform_filter_xss($data['extra']['field_suffix']) : $data['extra']['field_suffix']),
    '#description'    => $filter ? webform_filter_descriptions($data['extra']['description'], $node) : $data['extra']['description'],
    '#translatable'   => array('title', 'description'),
  );
  $element['size'] = array(
    '#type'           => 'select',
    '#title'          => t('Size'),
    '#title_display'  => 'before',
    '#options'        => $data['extra']['sizes'],
    '#default_value'  => $data['value'] ? $data['value'] : NULL,
    '#field_prefix'   => empty($data['extra']['field_prefix']) ? NULL : ($filter ? webform_filter_xss($data['extra']['field_prefix']) : $data['extra']['field_prefix']),
    '#field_suffix'   => empty($data['extra']['field_suffix']) ? NULL : ($filter ? webform_filter_xss($data['extra']['field_suffix']) : $data['extra']['field_suffix']),
    '#description'    => $filter ? webform_filter_descriptions($data['extra']['description'], $node) : $data['extra']['description'],
    '#translatable'   => array('title', 'description'),
  );
  return $element;
}

/**
 * Implements _webform_submit_component().
 */
function _webform_submit_double_select($component, $value) {
  $options = webform_double_select_options($component);
  $result = [];
  foreach ($value['double_select'] as $key => $optionValue) {
    if (!empty($optionValue) && !empty($options[$optionValue])) {
      $result[] = $optionValue;
    }
  }
  return $result;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_double_select($component, $value, $format = 'html', $submission = array()) {
  $result = array(
    '#title'          => 'Color & Size',
    '#title_display'  => 'before',
    '#weight'         => 1,
    '#theme'          => 'webform_display_double_select',
    '#theme_wrappers' => $format == 'html' ? array('webform_element') : array('webform_element_text'),
    '#format'         => $format,
    '#options'        => webform_double_select_options($component),
    '#value'          => $value,
    '#translatable'   => array('title', 'options'),
  );
  return $result;
}

/**
 * Format the text output for this component.
 */
function theme_webform_display_double_select($variables) {
  $element = $variables['element'];
  $options = array();
  $items = array();
  foreach ($element['#options'] as $key => $value) {
    $options[$key] = $value;
  }
  foreach ($element['#value'] as $selected) {
    if (!empty($options[$selected])) {
      $items[] = $element['#format'] == 'html' ? webform_filter_xss($options[$selected]) : $options[$selected];
    }
    else {
      $items[] = $element['#format'] == 'html' ? check_plain($selected) : $selected;
    }
  }
  if ($element['#format'] == 'html') {
    $output = count($items) > 1 ? theme('item_list', array('items' => $items)) : (!empty($items[0]) ? $items[0] : '');
  }
  else {
    if (count($items) > 1) {
      foreach ($items as $key => $item) {
        $items[$key] = ' - ' . $item;
      }
      $output = implode("\n", $items);
    }
    else {
      $output = isset($items[0]) ? $items[0] : '';
    }
  }
  return $output;
}

/**
 * Generate a list of options for a select list.
 */
function webform_double_select_options($component, $flat = FALSE, $filter = FALSE) {
  $options = array();
  if ($component['extra']['colors']) {
    $options += $component['extra']['colors'];
  }
  if ($component['extra']['sizes']) {
    $options += $component['extra']['sizes'];
  }
  return !empty($options) ? $options : array();
}