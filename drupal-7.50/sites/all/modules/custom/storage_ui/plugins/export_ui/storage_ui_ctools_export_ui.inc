<?php
/**
 * Created by PhpStorm.
 * User: riv
 * Date: 14.10.16
 * Time: 13:52
 */

$plugin = array(
  'schema' => 'storage_ui_preset',
  // As defined in hook_schema().
  'access' => 'administer storage_ui',
  // Define a permission users must have to access these pages.

  // Define the menu item.
  'menu' => array(
    'menu item' => 'storage_ui',
    'menu title' => 'Storage UI',
    'menu description' => 'Administer storage_ui presets.',
  ),

  // Define user interface texts.
  'title singular' => t('preset'),
  'title plural' => t('presets'),
  'title singular proper' => t('storage_ui preset'),
  'title plural proper' => t('storage_ui presets'),

  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'storage_ui_ctools_export_ui_form',
    // 'submit' and 'validate' are also valid callbacks.
  ),
);

function storage_ui_ctools_export_ui_form(&$form, &$form_state) {
  $preset = $form_state['item'];

  // Add Mymodule's configuration interface.
  $form['mydata'] = array(
    '#type' => 'textfield',
    '#title' => t('storage_ui configuration'),
    '#description' => t('This is just the simplest possible example of a configuration interface.'),
    '#default_value' => $preset->mydata,
    '#required' => TRUE,
  );
}
