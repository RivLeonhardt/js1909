/**
 * Created by riv on 06.10.16.
 */

(function ($) {
  Drupal.behaviors.ulColorPicker = {
    attach: function (context, settings) {
      $('#list', context).once(function () {
        $('li', context).click(
            function () {
              var color = color_rand();
              $(this).css('color', color);
            }
          );
        }
      );
    }
  };
})(jQuery);

function color_rand() {
  var color = ['white', 'red', 'blue', 'yellow', 'green', 'purple'];
  var number = Math.round(Math.random() * (5 - 1) + 1);
  return color[number];
}