<?php
/**
 * @file
 * View for pets owners storage.
 */

/**
 * Implements hook_views_data().
 */
function pets_owners_form_views_data() {
  $data = array();
  $data['pets_owners']['table']  = array(
    'group' => t('My custom view'),
    'base'  => array(
      'field' => 'id',
      'title' => t('Pets owners storage'),
    ),
  );
  $data['pets_owners']['id']     = array(
    'title' => t('ID'),
    'help'  => t('The unique ID of pets owners storage.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
   // views_handler_filter::
  );
  $data['pets_owners']['name']   = array(
    'title' => t('Name'),
    'help'  => t('Pets owners name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['pets_owners']['gender'] = array(
    'title' => t('gender'),
    'help'  => t('Pets owners name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['pets_owners']['prefix'] = array(
    'title' => t('prefix'),
    'help'  => t('Prefix.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'pets_owners_form_handler_filter_field_prefix',
    ),
  );
  $data['pets_owners']['age']    = array(
    'title' => t('Age'),
    'help'  => t('Age.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
  );
  $data['pets_owners']['father'] = array(
    'title' => t('Father'),
    'help'  => t('Fathers name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['pets_owners']['mother'] = array(
    'title' => t('Mother'),
    'help'  => t('Mothers name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['pets_owners']['pets']   = array(
    'title' => t('Pets'),
    'help'  => t('Pets names.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['pets_owners']['mail']   = array(
    'title' => t('Mail'),
    'help'  => t('User email.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  return $data;
}
