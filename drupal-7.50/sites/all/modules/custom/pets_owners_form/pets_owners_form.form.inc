<?php

/**
 * @file
 * Include form variables and DB operations (add, edit, delete field).
 */

/**
 * Implements hook_form().
 */
function pets_owners_form_form($form, &$form_state) {
  $form['messages'] = array(
    '#type'        => 'container',
    '#prefix'      => '<div id="custom-msg">',
    '#suffix'      => '</div>',
  );
  $form['name'] = array(
    '#type'    => 'textfield',
    '#title'   => t('name'),
    '#prefix'  => '<div id="myform">',
    //'#theme'   => 'custom_textfield',
    //'#process' => array('ren_text'),
  );
  $form['gender'] = array(
    '#type'    => 'radios',
    '#title'   => t('gender'),
    '#options' => array(
      'male'    => t('male'),
      'female'  => t('female'),
      'unknown' => t('unknown'),
    ),
    '#default_value' => 'unknown',
  );
  $form['prefix'] = array(
    '#type'    => 'select',
    '#title'   => t('prefix'),
    '#options' => array(
      'mr'  => 'mr',
      'mrs' => 'mrs',
      'ms'  => 'ms',
    ),
  );
  $form['age'] = array(
    '#type'    => 'textfield',
    '#title'   => t('age'),
    '#ajax'    => array(
      'callback' => 'ajax_show_parents_callback',
      'wrapper'  => 'textfields',
      'effect'   => 'fade',
    ),
  );
  $form['parents'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('parents'),
    '#collapsible' => TRUE,
    '#prefix'      => '<div id="textfields">',
    '#suffix'      => '</div>',
  );
  $form['parents']['father'] = array(
    '#type'     => 'textfield',
    '#title'    => t("father's name"),
    '#access'   => FALSE,
  );
  $form['parents']['mother'] = array(
    '#type'     => 'textfield',
    '#title'    => t("mother's name"),
    '#access'   => FALSE,
  );
  $form['pets_check'] = array(
    '#type'     => 'checkbox',
    '#title'    => t('Do you have pets?'),
  );
  $form['pets'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Do you have pets?'),
    '#states'   => array(
      'visible' => array(
        ':input[name="pets_check"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['mail'] = array(
    '#type'     => 'textfield',
    '#title'    => t('email address'),
  );
  $form['submit'] = array(
    '#type'     => 'submit',
    '#value'    => t('submit'),
    '#suffix'   => '</div>',
    '#ajax'     => array(
      'callback' => 'pets_owners_form_ajax_submit',
      'effect'   => 'fade',
      'method'   => 'html',
      'wrapper'  => 'myform',
    ),
  );
  return $form;
}
function ren_text($element) {
  $element['name'] = array(
    '#type'  => 'textfield',
  );
  return $element;
}


/**
 * Ajax callback to rebuild form after submit.
 *
 * @param array $form
 *   The node being added or edited.
 * @param array $form_state
 *    The form state array.
 *
 * @return array
 *    Return new form
 */
function pets_owners_form_ajax_submit($form, &$form_state) {
  $errors = form_get_errors();
  if (!empty($errors)) {
    $messages = theme_status_messages($errors);
    $commands[] = ajax_command_html('#custom-msg', $messages);
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $new_state = array();
    $new_state['build_info'] = $form_state['build_info'];
    $new_state['rebuild']    = TRUE;
    $new_state['values']     = array();
    $new_build  = drupal_rebuild_form('pets_owners_form_form', $new_state);
    $new_form   = drupal_render($new_build);
    $commands   = array();
    $commands[] = ajax_command_html('#myform', $new_form);
    $messages   = theme_status_messages(array('Thank you' => 'status'));
    $commands[] = ajax_command_html('#custom-msg', $messages);
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}

/**
 * Implements hook_form().
 */
function pets_owners_edit_form($form, &$form_state, $id) {
  $form = drupal_retrieve_form('pets_owners_form_form', $form_state);
  $fields = select_item('pets_owners', 'po', $id);
  $form['edit_id'] = array(
    '#type'  => 'hidden',
    '#value' => $id,
  );
  $form['name']['#value']               = $fields['name'];
  $form['gender']['#default_value']     = $fields['gender'];
  $form['prefix']['#value']             = $fields['prefix'];
  $form['age']['#value']                = $fields['age'];
  $form['parents']['father']['#access'] = TRUE;
  $form['parents']['mother']['#access'] = TRUE;
  $form['parents']['father']['#value']  = $fields['father'];
  $form['parents']['mother']['#value']  = $fields['mother'];
  $form['pets']['#value']               = $fields['pets'];
  $form['mail']['#value']               = $fields['mail'];
  $form['pets_check']['#default_value'] = 1;
  $form['#validate'][]                  = 'pets_owners_form_form_validate';
  $form['submit']['#submit']            = array('pets_owners_form_edit_submit');
  return $form;
}

/**
 * Implements hook_validate().
 */
function pets_owners_form_form_validate($form, &$form_state) {
  if (!empty($form_state['input']['name'])) {
    $name = preg_replace("/[^A-Za-z0-9 ]/", '', $form_state['input']['name']);
    $name = trim($name);
    if (strlen($name) >= 100) {
      form_set_error('name', t('name is too long'));
    }
  }
  if (isset($form_state['input']['age'])) {
    $validationErrors = validate_age($form_state['input']['age']);
    if ($validationErrors != NULL) {
      form_set_error('age', $validationErrors);
    }
  }
  if (!empty($form_state['input']['mail'])) {
    $mail = $form_state['input']['mail'];
    if (!valid_email_address($mail)) {
      form_set_error('mail', t('your email not valid'));
    }
  }
}

/**
 * Implements hook_submit().
 */
function pets_owners_form_form_submit($form, &$form_state) {
  if (add_owner($form_state['input']) != 0) {
    form_set_error('custom', 'error submitting form');
  }
  else {
    drupal_set_message('Thank you');
  }
}

/**
 * Implements hook_submit().
 */
function pets_owners_form_edit_submit($form, &$form_state) {
  $item = $form_state['input'];
  $queue = DrupalQueue::get('pets');
  $queue->createItem($item);
  $queue->claimItem(5);
  drupal_set_message(t('Edited'));
}

/**
 * Implements hook_form().
 */
function batch_upload_form($form, &$form_state) {
  $form['old'] = array(
    '#title'    => t('Old name'),
    '#type'     => 'textfield',
    '#required' => TRUE,
  );
  $form['new'] = array(
    '#title'    => t('New name'),
    '#type'     => 'textfield',
    '#required' => TRUE,
  );
  $form['submit'] = array(
    '#type'  => 'submit',
    '#value' => t('Upload'),
  );
  return $form;
}

/**
 * Implements hook_submit().
 */
function batch_upload_form_submit($form, &$form_state) {
  $old = $form_state['values']['old'];
  $new = $form_state['values']['new'];

  $batch = array(
    'operations' => array(
      array('batch_upload_process', array($old, $new)),
    ),
    'finished'         => 'batch_upload_finished',
    'title'            => t('Processing batch upload'),
    'init_message'     => t('Batch is starting.'),
    'progress_message' => t('Processed @current out of @total.'),
    'error_message'    => t('Batch has encountered an error.'),
    'file'             => drupal_get_path('module', 'pets_owners_form') . '/batch_upload.inc',
  );
  batch_set($batch);
}

/**
 * Create array() from non-empty fields and add record to table.
 *
 * @param array $rawData
 *   Raw data to clean and add to DB.
 * @param int $id
 *   Editing id.
 *
 * @return int
 *    Return zero if all OK, data added.
 */
function add_owner($rawData, $id = NULL) {
  $insertData = array();
  foreach ($rawData as $key => $value) {
    if (db_field_exists('pets_owners', $key) && !empty($value)) {
      $insertData[$key] = $value;
    }
  }
  if ($id == NULL) {
    db_insert('pets_owners')
      ->fields($insertData)
      ->execute();
    return 0;
  }
  else {
    db_merge('pets_owners')
      ->key(array('id' => $id))
      ->fields($insertData)
      ->execute();
    return 0;
  }
}

/**
 * Function to delete item from database.
 *
 * @param int $id
 *   Item id.
 *
 * @ingroup callbacks
 */
function pets_owners_delete_callback($id) {
  $result = delete_item('pets_owners', $id);
  if ($result) {
    $message = t('Item @id deleted', array('@id' => $id));
    drupal_set_message($message);
    drupal_goto('pets_owners_list');
  }
  else {
    drupal_set_message(t('Item does not exist'));
  }
}

/**
 * Example usage of drupal behavior.
 *
 * @return mixed
 *    Returns HTML for a list or nested list of items.
 */
function show_list_callback() {
  $items = array(
    array('Item 1'),
    array('Item 2'),
    array('Item 3'),
  );
  $form['theme_list'] = array(
    '#theme'      => 'item_list',
    '#items'      => $items,
    '#attributes' => array('id' => 'list'),
  );
  $form['#attached']['js'] = array(
    'js' => array(
      drupal_add_js(
        drupal_get_path('module', 'pets_owners_form') . '/js/color.js'
      ),
    ),
  );
  return $form;
}

/**
 * Perform validation on air when form field value 'AGE' is changed.
 *
 * @param array $form
 *    The form being added or edited.
 * @param array $form_state
 *    The form state array.
 *
 * @return string
 *    An array form elements to be displayed in the editing form.
 *
 * @ingroup callbacks
 */
function ajax_show_parents_callback($form, $form_state) {
  $form['parents']['father']['#access']   = FALSE;
  $form['parents']['mother']['#access']   = FALSE;
  $age = $form_state['input']['age'];
  $validationErrors = validate_age($age);
  if ($validationErrors != NULL) {
    form_set_error('age', $validationErrors);
  }
  elseif ($age < 18 && $age != '') {
    $form['parents']['father']['#access'] = TRUE;
    $form['parents']['mother']['#access'] = TRUE;
  }
  return $form['parents'];
}
