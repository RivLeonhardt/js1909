<?php

/**
 * @file
 * Handle batch upload process.
 */

/**
 * Batch upload callback.
 *
 * @param string $old_name
 *    Name to update.
 * @param string $new_name
 *    New name.
 *
 * @return int
 *    Return number of changed items (zero if none items).
 *
 * @ingroup callbacks
 */
function batch_upload_process($old_name, $new_name, &$context) {
  $max = count_names($old_name);
  if ($max == 0) {
    return $context['results'][] = $max;
  }
  if (empty($context['sandbox']['progress'])) {
    $context['sandbox']['progress']     = 0;
    $context['sandbox']['current_item'] = 0;
    $context['sandbox']['max']          = 1;
  }
  $value = upload_name($old_name, $new_name);
  $context['results'][] = $value;
  $context['sandbox']['progress']++;
  $context['sandbox']['current_item']++;
  $context['message'] = t('Now processing %item', array('%item' => 1));

  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Batch 'finished' callback.
 */
function batch_upload_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count items successfully processed:', array('@count' => $results[0]));
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t(
      'An error occurred while processing %error_operation with arguments: @arguments',
      array(
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      )
    );
    drupal_set_message($message, 'error');
  }
}

/**
 * Help func.
 *
 * @param string $old_name
 *    Name to update.
 * @param string $new_name
 *    New name.
 *
 * @return int
 *    Return.
 */
function upload_name($old_name, $new_name) {
  return db_update('pets_owners')
    ->fields(array('name' => $new_name))
    ->condition('name', $old_name, '=')
    ->execute();
}

/**
 * Help function to count identical names.
 *
 * @param string $name
 *    Name in database.
 *
 * @return int
 *    Count names.
 */
function count_names($name) {
  if (db_table_exists('pets_owners')) {
    $query = db_select('pets_owners', 'po')
      ->fields('po', array('name'))
      ->condition('name', $name, '=')
      ->execute()
      ->fetchAll();
    return count($query);
  }
  else {
    return 0;
  }
}
