<?php

/**
 * @file
 * Short desc.
 */

/**
 * Function retrieve all taxonomy terms in table (tid, name).
 *
 * @return mixed
 *   Returns assocArray of objects.
 */
function get_all_taxonomy_terms() {
  if ($cache = cache_get('all_taxonomy_terms')) {
    drupal_set_message(t('Used all taxonomy terms cache'));
    return $cache->data;
  }
  else {
    $items = db_query(
      'SELECT t.tid, ttd.name 
       FROM {taxonomy_index} t, {taxonomy_term_data} ttd
       WHERE t.tid = ttd.tid'
    )->fetchAllAssoc('tid');
    cache_set('all_taxonomy_terms', $items);
    drupal_set_message(t('Created all taxonomy terms cache'));
    return $items;
  }
}

/**
 * Function retrieve all nodes in form (nid, title, tid).
 *
 * @return mixed
 *   Returns assocArray of objects.
 */
function get_all_nodes_by_term(){
  if ($cache = cache_get('all_nodes_by_term')) {
    drupal_set_message(t('Used all nodes by term cache'));
    return $cache->data;
  }
  else {
    $items = db_query(
      'SELECT n.nid, n.title, t.tid 
       FROM {node} n 
       LEFT JOIN {taxonomy_index} t
        ON t.nid = n.nid 
       ORDER BY t.tid'
    )->fetchAllAssoc('nid');
    cache_set('all_nodes_by_term', $items);
    drupal_set_message(t('Create all nodes by term cache'));
    return $items;
  }
}

/**
 * Function to create list scheme.
 *
 * @param mixed $taxonomyTerms
 *   List of all taxonomy terms.
 * @param mixed $nodesTitles
 *   List of all nodes in format (nid, title, tid)
 *
 * @return array
 *    Returns array for theme_item_list.
 */
function create_list_scheme($taxonomyTerms, $nodesTitles) {
  $data = array(
    'NC' => array(
      'data' => 'No Category',
      'children' => array(),
    ),
  );
  foreach ($taxonomyTerms as $term) {
    $data[$term->tid] = array(
      'data' => $term->name,
    );
  }
  foreach ($nodesTitles as $node) {
    if ($node->tid != NULL) {
      $data[$node->tid]['children'][$node->nid] = array('data' => $node->title);
    }
    else {
      $data['NC']['children'][$node->nid] = array('data' => $node->title);
    }
  }
  return $data;
}

function delete_item($table, $id) {
  return db_delete($table)->condition('id', $id, '=')->execute();
}

function select_item($table, $alias, $id) {
  $result = db_select($table, $alias)
    ->fields($alias)
    ->condition('id', $id, '=')
    ->execute()
    ->fetchAssoc();
  return $result;
}

/**
 * Validation function, check if age is NUMERIC and within range [0..18].
 *
 * @param string $age
 *    Input string.
 *
 * @return string
 *    string with error message or NULL if all OK.
 */
function validate_age($age) {
  if ($age == '') {
    return NULL;
  }
  elseif (is_numeric($age)) {
    if ($age <= 0 || $age > 120) {
      return t("wrong age");
    }
  }
  else {
    return t("enter numeric value");
  }
}
