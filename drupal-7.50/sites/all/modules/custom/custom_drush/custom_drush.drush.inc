<?php

/**
 * @file
 * Custom drush command.
 */

/**
 * Implements hook_drush_command().
 */
function custom_drush_drush_command() {
  $items['unpublish-nodes'] = array(
    'description' => 'Demonstrate how Drush commands work.',
    'aliases' => array('un'),
    'callback'  => 'unpublish_nodes',
    'arguments' => array(
      'days'  => 'number of days, default 7',
    ),
  );
  return $items;
}

/**
 * Unpublish nodes older than $days.
 */
function unpublish_nodes($days = 7) {
  $nodes = node_load_multiple(FALSE);
  $today = new DateTime();
  foreach ($nodes as $node) {
    $created = new DateTime(date('Ymd h:i:s', $node->created));
    $interval = $created->diff($today)->days;
    if ($interval > $days) {
      $node->status = 0;
      node_save($node);
    }
  }
  drupal_set_message(t("Nodes older than @days days were unpublished", array('@days' => $days)));
}
