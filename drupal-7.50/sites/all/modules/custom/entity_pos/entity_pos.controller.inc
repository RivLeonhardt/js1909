<?php

/**
 * @file
 * Contains controllers for entity.
 */

/**
 * Storage class.
 */
class Storage extends Entity {

  /**
   * DefaultLabel.
   *
   * @return string
   *    Default label is entity sid.
   */
  protected function defaultLabel() {
    return $this->sid;
  }

  /**
   * DefaultUri.
   *
   * @return string
   *    Default path.
   */
  protected function defaultUri() {
    return array('path' => 'storage/' . $this->identifier());
  }

}

/**
 * Storage Type class.
 */
class StorageType extends Entity {
  public $type;
  public $label;

  /**
   * StorageType constructor.
   *
   * @param array $values
   *    Values.
   */
  public function __construct($values = array()) {
    parent::__construct($values, 'storage_type');
  }

}

/**
 * UI controller for Storage Type.
 */
class StorageTypeUIController extends EntityDefaultUIController {

  /**
   * Overrides hook_menu() defaults.
   */
  public function hook_menu() {
    $items = parent::hook_menu();
    $items[$this->path]['description'] = 'Manage Storage types.';
    return $items;
  }

}

/**
 * The Controller for Model entities.
 */
class StorageController extends EntityAPIController {

  /**
   * Storage create.
   *
   * @param array $values
   *    Values.
   *
   * @return object
   *    Create new storage.
   */
  public function create(array $values = array()) {
    global $user;
    $values += array(
      'description' => '',
      'created'     => REQUEST_TIME,
      'changed'     => REQUEST_TIME,
      'uid'         => $user->uid,
      'name'        => 'default_name',
      'gender'      => 'unknown',
      'prefix'      => 'mr',
      'age'         => '1',
      'father'      => '',
      'mother'      => '',
      'pets'        => '',
      'mail'        => '',
    );
    return parent::create($values);
  }

  /**
   * Build content.
   *
   * @param mixed $entity
   *   Entity.
   * @param string $view_mode
   *    View mode = full.
   * @param string $langcode
   *    Language.
   * @param array $content
   *    Content.
   *
   * @return array
   *    Return content.
   */
  public function buildContent($entity, $view_mode = 'full', $langcode = NULL, $content = array()) {
    $wrapper = entity_metadata_wrapper('storage', $entity);
    $content['User'] = array(
      '#markup' => t(
        'Created by: @user', array(
          '@user' => $wrapper->uid->name->value(
            array('sanitize' => TRUE)
          ),
        )
      ),
    );
    return parent::buildContent($entity, $view_mode, $langcode, $content);
  }

}

/**
 * The Controller for Model entities.
 */
class StorageTypeController extends EntityAPIControllerExportable {

  /**
   * Storage type create.
   *
   * @param array $values
   *    Values.
   *
   * @return object
   *    Create new StorageType object.
   */
  public function create(array $values = array()) {
    $values += array(
      'label' => '',
      'description' => '',
    );
    return parent::create($values);
  }

  /**
   * Save Storage Type.
   */
  public function save($entity, DatabaseTransaction $transaction = NULL) {
    parent::save($entity, $transaction);
    variable_set('menu_rebuild_needed', TRUE);
  }

}
