<?php

/**
 * @file
 * Storage entity REST CRUD.
 */

/**
 * REST API create.
 *
 * @param array $data
 *    Data for entity.
 *
 * @return string
 *    Success string.
 */
function storage_resource_create($data) {
  $storage = _services_arg_value($data, 'storage');
  storage_resource_validate_type($storage);
  $default_storage = entity_create('storage', array('type' => $storage['type']));
  $form_state      = entity_form_state($storage, 'Save storage');
  module_load_include('inc', 'entity_pos', 'entity_pos.admin');
  drupal_form_submit('storage_form', $form_state, $default_storage);
  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  return t('Storage created');
}

/**
 * REST API retrieve.
 *
 * @param int $sid
 *    Storage unique id.
 *
 * @return mixed
 *    Return entity.
 */
function storage_resource_retrieve($sid) {
  if (!is_numeric($sid)) {
    return services_error(t('Input numeric value'), 406);
  }
  else {
    $storage = storage_load($sid);
    return $storage;
  }
}

/**
 * REST API update.
 *
 * @param int $sid
 *    Storage ID.
 * @param array $data
 *    Input data.
 *
 * @return string
 *    Abc.
 */
function storage_resource_update($sid, $data) {
  $storage = _services_arg_value($data, 'storage');
  storage_resource_validate_type($storage);
  $default_storage = storage_load($sid);
  $form_state      = entity_form_state($storage, 'Save storage');
  module_load_include('inc', 'entity_pos', 'entity_pos.admin');
  drupal_form_submit('storage_form', $form_state, $default_storage);
  if ($errors = form_get_errors()) {
    return services_error(implode(" ", $errors), 406, array('form_errors' => $errors));
  }
  return t('Storage edited');
}

/**
 * REST API delete.
 *
 * @param int $sid
 *    Storage unique id.
 *
 * @return string
 *    String with deleted item.
 */
function storage_resource_delete($sid) {
  if (!is_numeric($sid)) {
    return services_error(t('Input numeric value'), 406);
  }
  else {
    storage_delete_multiple(array($sid));
    return t("Storage @sid deleted", array('@sid' => $sid));
  }
}

/**
 * REST API index.
 */
function storage_resource_index($page, $fields, $parameters, $page_size, $options = array()) {
  $storage_select = db_select('storage', 't');
  services_resource_build_index_query($storage_select, $page, $fields, $parameters, $page_size, 'storage', $options);
  $results = services_resource_execute_index_query($storage_select);
  return services_resource_build_index_list($results, 'storage', 'sid');
}

/**
 * Helper function to validate storage type information.
 *
 * @param array $storage
 *    Array representing the attributes a node edit form would submit.
 *
 * @return mixed
 *    Exceptions.
 */
function storage_resource_validate_type($storage) {
  if (empty($storage['type'])) {
    return services_error(t('Missing storage type'), 406);
  }
  $types = storage_type_types();
  $storage_type = $storage['type'];
  if (empty($types[$storage_type])) {
    return services_error(
      t('Storage type @type does not exist.', array('@type' => $storage_type)), 406);
  }
  $allowed_storage_types = variable_get('services_allowed_create_content_types', $types);
  if (empty($allowed_storage_types[$storage_type])) {
    return services_error(
      t("This storage type @type can't be processed via services", array('@type' => $storage_type)), 406
    );
  }
}

/**
 * Helper function to create form_state.
 *
 * @param array $values
 *    Form values.
 * @param string $op
 *    Operation.
 *
 * @return array
 *    New form state.
 */
function entity_form_state($values, $op) {
  $form_state = array();
  $form_state['values']                         = $values;
  $form_state['values']['op']                   = $op;
  $form_state['programmed_bypass_access_check'] = FALSE;
  $form_state['no_cache']                       = TRUE;
  return $form_state;
}
