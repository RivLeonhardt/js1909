<?php

class entity_pos_handler_filter_field_prefix extends views_handler_filter_string {

  public function operators() {
    $operators = array(
      '=' => array(
        'title'  => t('Is equal to'),
        'short'  => t('='),
        'method' => 'op_equal',
        'values' => 1,
      ),
    );
    return $operators;
  }

  public function value_form(&$form, &$form_state) {
    parent::value_form($form, $form_state);
    $form['value']['#type'] = 'select';
    $form['value']['#options'] = array('male' => 'male', 'female' => 'female');
    $form['value']['#size'] = 1;
  }

  public function op_equal($field) {
    $gender = $this->value;
    if ($gender == 'male') {
      $this->query->add_where($this->options['group'], $field, 'mr', $this->operator());
    }
    else {
      $or = db_or()->condition($field, 'mrs', $this->operator());
      $this->query->add_where(
        $this->options['group'],
        $or->condition($field, 'ms', $this->operator())
      );
    }
  }

}
