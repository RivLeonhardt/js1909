<?php
/**
 * @file
 * Abc.
 */

/**
 * Implements hook_views_data().
 */
function entity_pos_views_data() {
  $data = array();
  $data['storage']['table']  = array(
    'group' => t('My entity view'),
    'base'  => array(
      'field' => 'sid',
      'title' => t('storage'),
    ),
    'access query tag' => 'storage_access',
    'entity type' => 'storage',
  );
  $data['storage']['sid']    = array(
    'title' => t('SID'),
    'help'  => t('The unique Storage ID of pets owners storage.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
    'sort'     => array(
      'handler' => 'views_handler_sort',
    ),
    // views_handler_filter::
  );
  $data['storage']['name']   = array(
    'title' => t('Name'),
    'help'  => t('Pets owners name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['storage']['gender'] = array(
    'title' => t('gender'),
    'help'  => t('Pets owners name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['storage']['prefix'] = array(
    'title' => t('prefix'),
    'help'  => t('Prefix.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'filter' => array(
      'handler' => 'entity_pos_handler_filter_field_prefix',
    ),
  );
  $data['storage']['age']    = array(
    'title' => t('Age'),
    'help'  => t('Age.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
  );
  $data['storage']['father'] = array(
    'title' => t('Father'),
    'help'  => t('Fathers name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['storage']['mother'] = array(
    'title' => t('Mother'),
    'help'  => t('Mothers name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['storage']['pets']   = array(
    'title' => t('Pets'),
    'help'  => t('Pets names.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  $data['storage']['mail']   = array(
    'title' => t('Mail'),
    'help'  => t('User email.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
  );
  return $data;
}
