<?php

/**
 * @file
 * Contains database schema description.
 */

/**
 * Implements hook_schema().
 */
function entity_pos_schema() {
  $schema = array();
  $schema['storage'] = array(
    'description' => 'Table to store data',
    'fields' => array(
      'sid' => array(
        'description' => "The identifier for owner",
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'type' => array(
        'description' => 'The type (bundle) of this storage.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'uid' => array(
        'description' => 'ID of Drupal user creator.',
        'type' => 'int',
        'not null' => TRUE,
      ),
      'created' => array(
        'description' => 'The Unix timestamp when the storage was created.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'changed' => array(
        'description' => 'The Unix timestamp when the storage was most recently saved.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'name' => array(
        'description' => "owner's name",
        'type' => 'varchar',
        'length' => '100',
        'default' => '',
      ),
      'gender' => array(
        'description' => "owner's gender",
        'type' => 'varchar',
        'length' => '12',
        'default' => 'unknown',
      ),
      'prefix' => array(
        'description' => "prefix",
        'type' => 'varchar',
        'length' => '6',
        'default' => 'mr',
      ),
      'age' => array(
        'description' => "owner's age",
        'type' => 'int',
        'default' => '1',
        'unsigned' => TRUE,
      ),
      'father' => array(
        'description' => "parents names",
        'type' => 'varchar',
        'length' => '100',
        'default' => '',
      ),
      'mother' => array(
        'description' => "parents names",
        'type' => 'varchar',
        'length' => '100',
        'default' => '',
      ),
      'pets' => array(
        'description' => "parents names",
        'type' => 'varchar',
        'length' => '100',
        'default' => '',
      ),
      'mail' => array(
        'description' => "owner's mail",
        'type' => 'varchar',
        'length' => '100',
        'default' => '',
      ),
    ),
    'primary key' => array('sid'),
    'indexes' => array('sid' => array('sid')),
  );
  $schema['storage_type'] = array(
    'description' => 'Stores info about bundle pets_storage',
    'fields' => array(
      'id' => array(
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique storage type ID.',
      ),
      'type' => array(
        'description' => 'The machine-readable name of this type.',
        'type' => 'varchar',
        'length' => 32,
        'not null' => TRUE,
      ),
      'label' => array(
        'description' => 'The human-readable name of this type.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ),
      'description' => array(
        'description' => 'A brief description of this type.',
        'type' => 'text',
        'not null' => TRUE,
        'size' => 'medium',
        'translatable' => TRUE,
      ),
    ) + entity_exportable_schema_fields(),
    'primary key' => array('id'),
    'unique keys' => array(
      'type' => array('type'),
    ),
  );
  return $schema;
}
