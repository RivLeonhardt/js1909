<?php

/**
 * Implements hook_action_info().
 */
function entity_pos_change_gender_to_female_action_info() {
  return array(
    'entity_pos_change_gender_to_female_action' => array(
      'type'             => 'storage',
      'label'            => t('Change gender to female'),
      'behavior'         => array('edit storage entities'),
      'configurable'     => FALSE,
      'vbo_configurable' => FALSE,
      'triggers'         => array('any'),
    ),
  );
}


function entity_pos_change_gender_to_female_action(&$entity, $context) {
  if ($entity->gender != 'female') {
    $entity->gender = 'female';
    storage_save($entity);
  }
}
