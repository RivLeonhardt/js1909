<?php

/**
 * Implements hook_action_info().
 */
function entity_pos_change_gender_to_male_action_info() {
  return array(
    'entity_pos_change_gender_to_male_action' => array(
      'type'             => 'storage',
      'label'            => t('Change gender to male'),
      'behavior'         => array('edit storage entities'),
      'configurable'     => FALSE,
      'vbo_configurable' => FALSE,
      'triggers'         => array('any'),
    ),
  );
}


function entity_pos_change_gender_to_male_action(&$entity, $context) {
  if ($entity->gender != 'male') {
    $entity->gender = 'male';
    storage_save($entity);
  }
}
