<?php

/**
 * @file
 * Admin UI for storage entity.
 */

/**
 * Form for creating storage_type.
 */
function storage_type_form($form, &$form_state, $storage_type, $op = 'edit') {

  if ($op == 'clone') {
    $storage_type->label .= ' (cloned)';
    $storage_type->type = '';
  }
  $form['label'] = array(
    '#title' => t('Label'),
    '#type' => 'textfield',
    '#default_value' => $storage_type->label,
    '#description' => t('The human-readable name of this storage type.'),
    '#required' => TRUE,
    '#size' => 30,
  );
  $form['type'] = array(
    '#type' => 'machine_name',
    '#default_value' => isset($storage_type->type) ? $storage_type->type : '',
    '#maxlength' => 32,
    '#machine_name' => array(
      'exists' => 'storage_type_types',
      'source' => array('label'),
    ),
    '#description' => t('A unique machine-readable name for this storage type. It must only contain lowercase letters, numbers, and underscores.'
    ),
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#default_value' => isset($storage_type->description) ? $storage_type->description : '',
    '#description' => t('Description about the storage type.'),
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save storage type'),
    '#weight' => 40,
  );
  return $form;
}

/**
 * Submit handler for creating/editing storage_type.
 */
function storage_type_form_submit(&$form, &$form_state) {
  $storage_type = entity_ui_form_submit_build_entity($form, $form_state);
  storage_type_save($storage_type);
  $form_state['redirect'] = 'admin/structure/storage-types';
}

/**
 * Submit handler for deleting storage_type.
 */
function storage_type_form_submit_delete(&$form, &$form_state) {
  $form_state['redirect'] = 'admin/structure/storage-types/' . $form_state['storage_type']->type . '/delete';
}

/**
 * Storage type delete form.
 */
function storage_type_form_delete_confirm($form, &$form_state, $storage_type) {
  $form_state['storage_type'] = $storage_type;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['storage_type_id'] = array('#type' => 'value', '#value' => entity_id('storage_type', $storage_type));
  return confirm_form($form,
    t('Are you sure you want to delete storage type %title?', array('%title' => entity_label('storage_type', $storage_type))),
    'storage/' . entity_id('storage_type', $storage_type),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Storage type delete form submit handler.
 */
function storage_type_form_delete_confirm_submit($form, &$form_state) {
  $storage_type = $form_state['storage_type'];
  storage_type_delete($storage_type);

  watchdog('storage_type', '@type: deleted %title.', array('@type' => $storage_type->type, '%title' => $storage_type->label));
  drupal_set_message(t('@type %title has been deleted.', array('@type' => $storage_type->type, '%title' => $storage_type->label)));

  $form_state['redirect'] = 'admin/structure/storage-types';
}


/**
 * Menu callback.
 *
 * @return mixed
 *    Create table with all storage entities.
 */
function storage_list() {
  $rows = storage_to_list();
  $header = array(
    t('sid'),
    t('type'),
    t('name'),
    t('edit'),
    t('del'),
  );
  $table = theme('table', array('header' => $header, 'rows' => $rows));
  return $table;
}

/**
 * Object to list.
 *
 * @return array
 *    Rows for table.
 */
function storage_to_list() {
  $all = storage_load_multiple(FALSE);
  $rows = array();
  foreach ($all as $entity) {
    $rows[] = array(
      'data' => array(
        'sid'  => $entity->sid,
        'type' => $entity->type,
        'name' => $entity->name,
        'link' => l(t('edit'), 'storage/' . $entity->sid . '/edit'),
        'del'  => l(t('delete'), 'storage/' . $entity->sid . '/delete'),
      ),
    );
  }
  return $rows;
}

/**
 * Page to select and create Storage type.
 */
function storage_admin_add_page() {
  $items = array();
  foreach (storage_type_types() as $storage_type_key => $storage_type) {
    $items[] = l(entity_label('storage_type', $storage_type), 'storage/add/' . $storage_type_key);
  }
  return array(
    'list' => array(
      '#theme' => 'item_list',
      '#items' => $items,
      '#title' => t('Select type of storage to create.'),
    ),
  );
}

/**
 * Add new storage page.
 */
function storage_add($type) {
  $storage_type = storage_type_types($type);
  $storage = entity_create('storage', array('type' => $type));
  drupal_set_title(t('Create @name', array('@name' => entity_label('storage_type', $storage_type))));
  $output = drupal_get_form('storage_form', $storage);
  return $output;
}

/**
 * View entity.
 *
 * @param array $storage
 *    Entity.
 *
 * @return array
 *    Renderable array.
 */
function storage_view($storage) {
  return entity_view(
    'storage', array(entity_id('storage', $storage) => $storage), 'full'
  );
}

/**
 * Storage Form.
 */
function storage_form($form, &$form_state, $storage) {
  $form_state['storage'] = $storage;
  $form['name'] = array(
    '#type'    => 'textfield',
    '#title'   => t('name'),
    '#default_value' => $storage->name,
  );
  $form['gender'] = array(
    '#type'    => 'radios',
    '#title'   => t('gender'),
    '#options' => array(
      'male'    => t('male'),
      'female'  => t('female'),
      'unknown' => t('unknown'),
    ),
    '#default_value' => $storage->gender,
  );
  $form['prefix'] = array(
    '#type'    => 'select',
    '#title'   => t('prefix'),
    '#options' => array(
      'mr'  => 'mr',
      'mrs' => 'mrs',
      'ms'  => 'ms',
    ),
    '#default_value' => $storage->prefix,
  );
  $form['age'] = array(
    '#type'    => 'textfield',
    '#title'   => t('age'),
    '#default_value' => $storage->age,
  );
  $form['parents'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('parents'),
    '#collapsible' => TRUE,
  );
  $form['parents']['father'] = array(
    '#type'     => 'textfield',
    '#title'    => t("father's name"),
    '#default_value' => $storage->father,
  );
  $form['parents']['mother'] = array(
    '#type'     => 'textfield',
    '#title'    => t("mother's name"),
    '#default_value' => $storage->mother,
  );
  $form['pets_check'] = array(
    '#type'     => 'checkbox',
    '#title'    => t('Do you have pets?'),
  );
  $form['pets'] = array(
    '#type'     => 'textfield',
    '#title'    => t('Do you have pets?'),
    '#states'   => array(
      'visible' => array(
        ':input[name="pets_check"]' => array('checked' => TRUE),
      ),
    ),
    '#default_value' => $storage->pets,
  );
  $form['mail'] = array(
    '#type'     => 'textfield',
    '#title'    => t('email address'),
    '#default_value' => $storage->mail,
  );
  $form['uid'] = array(
    '#type' => 'value',
    '#value' => $storage->uid,
  );
  field_attach_form('storage', $storage, $form, $form_state);
  $submit = array();
  if (!empty($form['#submit'])) {
    $submit += $form['#submit'];
  }
  $form['actions'] = array(
    '#weight' => 100,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save storage'),
    '#submit' => $submit + array('storage_form_submit'),
  );
  $storage_id = entity_id('storage', $storage);
  if (!empty($storage_id) && storage_access('edit', $storage)) {
    $form['actions']['delete'] = array(
      '#type'   => 'submit',
      '#value'  => t('Delete'),
      '#submit' => array('storage_form_submit_delete'),
    );
  }
  return $form;
}

/**
 * Storage submit handler.
 */
function storage_form_submit($form, &$form_state) {
  $storage = $form_state['storage'];
  entity_form_submit_build_entity('storage', $storage, $form, $form_state);
  storage_save($storage);
  $storage_uri = entity_uri('storage', $storage);
  $form_state['redirect'] = $storage_uri['path'];
  drupal_set_message(
    t(
      'storage %title saved.',
      array('%title' => entity_label('storage', $storage))
    )
  );
}

/**
 * Storage delete submit handler.
 */
function storage_form_submit_delete($form, &$form_state) {
  $storage = $form_state['storage'];
  $storage_uri = entity_uri('storage', $storage);
  $form_state['redirect'] = $storage_uri['path'] . '/delete';
}

/**
 * Delete confirmation form.
 */
function storage_delete_form($form, &$form_state, $storage) {
  $form_state['storage'] = $storage;
  $form['storage_type_id'] = array(
    '#type' => 'value',
    '#value' => entity_id('storage', $storage),
  );
  $storage_uri = entity_uri('storage', $storage);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => entity_label('storage', $storage))),
    $storage_uri['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete form submit handler.
 */
function storage_delete_form_submit($form, &$form_state) {
  $storage = $form_state['storage'];
  storage_delete($storage->sid);
  drupal_set_message(t('Storage %title deleted.', array('%title' => entity_label('storage', $storage))));
  $form_state['redirect'] = 'storage';
}
