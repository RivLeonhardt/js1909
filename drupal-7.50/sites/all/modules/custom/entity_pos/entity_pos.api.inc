<?php

/**
 * @file
 * API.
 */

/**
 * Access callback for storage.
 */
function storage_access($op, $storage, $account = NULL, $entity_type = NULL) {
  global $user;

  if (empty($account)) {
    $account = $user;
  }
  switch ($op) {
    case 'create':
      return user_access('administer storage types', $account) || user_access('create storage entities', $account);

    case 'view':
      return user_access('administer storage types', $account) || user_access('view storage entities', $account);

    case 'edit':
      return user_access('administer storage types') || user_access('edit storage entities');
  }
}

/**
 * Load a storage.
 */
function storage_load($sid, $reset = FALSE) {
  $storage = storage_load_multiple(array($sid), array(), $reset);
  return reset($storage);
}

/**
 * Load multiple storage based on certain conditions.
 */
function storage_load_multiple($sids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('storage', $sids, $conditions, $reset);
}

/**
 * Save storage.
 */
function storage_save($storage) {
  entity_save('storage', $storage);
}

/**
 * Delete single storage.
 */
function storage_delete($sid) {
  entity_delete_multiple('storage', array($sid));
}

/**
 * Delete multiple storage.
 */
function storage_delete_multiple($storage_ids) {
  entity_delete_multiple('storage', $storage_ids);
}

/**
 * Access callback for Storage Type.
 */
function storage_type_access($op, $entity = NULL) {
  return user_access('administer storage types');
}

/**
 * Load Storage Type.
 */
function storage_type_load($storage_type) {
  return storage_type_types($storage_type);
}

/**
 * List of Storage Types.
 */
function storage_type_types($storage_type = NULL) {
  $types = entity_load_multiple_by_name('storage_type', isset($storage_type) ? array($storage_type) : FALSE);
  return isset($storage_type) ? reset($types) : $types;
}

/**
 * Save Storage entity.
 */
function storage_type_save($storage_type) {
  entity_save('storage_type', $storage_type);
}

/**
 * Delete single Storage type.
 */
function storage_type_delete($storage_type) {
  entity_delete('storage_type', entity_id('storage_type', $storage_type));
}

/**
 * Delete multiple Storage types.
 */
function storage_type_delete_multiple($storage_type_ids) {
  entity_delete_multiple('storage_type', $storage_type_ids);
}
