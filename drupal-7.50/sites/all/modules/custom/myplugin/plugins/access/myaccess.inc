<?php

/**
 * @file
 * Access plugin.
 */

$plugin = array(
  'title'         => t('Only for anonymous'),
  'description'   => t('Checks if you want to show it only to anon users.'),
  'callback'      => 'anon_ctools_access_check',
  'summary'       => 'anon_ctools_access_summary',
  'settings form' => 'anon_ctools_access_settings',
  'all contexts'  => TRUE,
);

/**
 * Settings for plugin.
 */
function anon_ctools_access_settings($form, &$form_state, $conf) {
  $form['settings']['anon'] = array(
    '#title'          => t('Show only for anonymous users'),
    '#type'           => 'checkbox',
    '#description'    => t('Show only for anonymous users.'),
    '#default_value'  => $conf['anon'],
    '#required'       => TRUE,
  );
  return $form;
}

/**
 * Main access callback.
 */
function anon_ctools_access_check($conf, $context) {
  $access = $conf['anon'];
  if ($access == 1 && user_is_anonymous()) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Summary callback for access plugin.
 */
function anon_ctools_access_summary($conf, $context) {
  return t('anonymous');
}
