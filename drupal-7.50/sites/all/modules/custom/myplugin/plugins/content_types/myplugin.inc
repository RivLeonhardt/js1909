<?php

/**
 * @file
 * Content plugin.
 */

$plugin = array(
  'single'          => TRUE,
  'title'           => t('Custom'),
  'description'     => t('Custom content plugin'),
  'category'        => t('Custom'),
  'edit form'       => 'myplugin_edit_form',
  'render callback' => 'myplugin_render',
  'all contexts'    => TRUE,
  'admin info'      => 'myplugin_admin_info',
  'defaults'        => array('name' => t('test name')),
);

/**
 * Render callback for content pane.
 *
 * @param $subtype
 * @param $conf
 * @param $args
 * @param $contexts
 *
 * @return \stdClass
 */
function myplugin_render($subtype, $conf, $args, $contexts) {
  $block          = new stdClass();
  $block->title   = 'Hi, name is:';
  $block->content = $conf['name'];

  return $block;
}

function myplugin_edit_form($form, &$form_state) {
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('What is your name?'),
    '#default_value' => $form_state['conf']['name'],
  );
  return $form;
}

/**
 * Edit form callback for custom content.
 *
 * @param array $form
 *   Form.
 * @param array $form_state
 *   State.
 */
function myplugin_edit_form_submit(&$form, &$form_state) {
  if (isset($form_state['values']['name'])) {
    $form_state['conf']['name'] = $form_state['values']['name'];
  }
}