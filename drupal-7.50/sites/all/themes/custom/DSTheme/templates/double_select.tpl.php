<?php

/**
 * @file
 * Double select component template. ONLY FORM.
 */
?>

<div class="template-test">
  <?php print render($variables['element']['size']) ?>
</div>
<div class="template-test2">
  <?php print render($variables['element']['color']) ?>
</div>
<?php print drupal_render_children($variables['element']) ?>
