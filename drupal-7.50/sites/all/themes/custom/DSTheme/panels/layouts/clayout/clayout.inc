<?php

// Plugin definition
$plugin = array(
  'title' => t('custom two'),
  'category' => t('Columns: 4'),
  'icon' => 'clayout.png',
  'theme' => 'panels_clayout',
  'css' => 'clayout.css',
  'regions' => array(
    'left' => t('Left side'),
    'right' => t('Right side')
  ),
);
