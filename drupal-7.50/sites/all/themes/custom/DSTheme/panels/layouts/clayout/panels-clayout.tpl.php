<?php
/**
 * Created by PhpStorm.
 * User: riv
 * Date: 27.09.16
 * Time: 14:13
 */
?>
<div class="panel-display panel-2col clearfix" <?php if (!empty($css_id)) {
  print "id=\"$css_id\"";
} ?>>
<div class="panel-panel panel-col-first">
  custom layout left
  <div class="inside"><?php print $content['left']; ?></div>
</div>

<div class="panel-panel panel-col-last">
  custom layout right
  <div class="inside"><?php print $content['right']; ?></div>
</div>
</div>