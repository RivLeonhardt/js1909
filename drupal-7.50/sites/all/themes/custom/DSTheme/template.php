<?php
/**
 * Created by PhpStorm.
 * User: riv
 * Date: 28.09.16
 * Time: 11:26
 */

/**
 * Override or insert variables into the node template.
 */
function dstheme_preprocess_node(&$variables) {

  if ($variables['node']->type == "page") {
    $entity = entity_metadata_wrapper('node', $variables['node']);
    if (!empty($entity->field_template)) {
      $variables['theme_hook_suggestions'][] = 'node__page__' . $entity->field_template->value();
    }
  }
  if ($variables['view_mode'] == 'full' && node_is_page($variables['node'])) {
    $variables['classes_array'][] = 'node-full';
  }
}

function dstheme_theme() {
  $items = array();
  $items['user_login'] = array(
    'render element' => 'form',
    'path' => drupal_get_path('theme', 'dstheme') . '/templates',
    'template' => 'user-login',
    'preprocess functions' => array(
      'dstheme_preprocess_user_login',
    ),
  );
  return $items;
}


function dstheme_preprocess_user_login(&$vars) {
  $vars['test'] = t('some text to test');
}


