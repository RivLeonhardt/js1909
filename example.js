


function SummonEvent(e) {
    "use strict";
    if (e.target.tagName === "INPUT") {
        var event = new CustomEvent("CustomEvent");
        e.target.dispatchEvent(event);
    }
}

function validateForm() {
    "use strict";
    var validationForm = document.forms.form;
    var errorMsg = document.getElementById("errorMsg");
    for (var i = 0; i < validationForm.length - 1; i++) {

        if (validationForm[i].value.trim().length == 0) {
            validationForm.submit.disabled = true;
            errorMsg.style.display = "";
            return false;
        }
    }
    errorMsg.style.display = "none";
    validationForm.submit.disabled = false;
    return true;
}

function ready() {
    var form = document.querySelector("#form");
    form.addEventListener("keyup", SummonEvent, false);

    var formItems = document.querySelectorAll(".regItem");
    for (var i = 0; i < formItems.length; i++) {
        form[i].addEventListener("CustomEvent", validateForm, false);
    }
    validateForm();
}

document.addEventListener("DOMContentLoaded", ready);